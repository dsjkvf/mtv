mtv
===


## About

`mtv` is a small Bash script that will present entries from M3U playlists in a `fzf` menu, allowing a user to select quickly an entry, thus opening it in a user-defined media player. The playlists in question should be located in a predefined `$FOLDER` directory, and an additional `$FILTER` file can be applied to narrow down the resulting list (an example of such file can be found in the `extras/` directory, named `filter.txt`). Options mentioned above are set in the body of the script.

`mtv-cat`, on the other hand, is a simplified version of the above script, which should only be used in pipes.


## Dependencies

`mtv` naturally depends on [`fzf`](https://github.com/junegunn/fzf), [GNU Awk](https://www.gnu.org/software/gawk/) and -- optionally -- on [`cyr2lat`](https://bitbucket.org/dsjkvf/cyr2lat/src/master/) (for romanizing Cyrillic entries in the playlists).


## Usage

Just edit the options and then use it like this:

    mtv

Or like this:

    cat 00.m3u8 |  mtv-cat

`mtv` also supports `--list` and `--list-all` command line options, which will simply print out the list of channels, filtered or unfiltered, to STDIN.


## Extras

Also, two helper scripts can be found in the `extras` folder, which illustrate how to update playlists if needed, either based on a timestamp of a remote file, or just periodically.These helper scripts should be stored in the same folder and have the same name as the corresponding playlists.
