#!/bin/bash

# OPTIONS

# Set the location
URL='https://location/of/a/timestamp.m3u'

# HELPERS

# Download and update
function down_upd() {
    curl -k -o ${SCRIPT/sh/m3u} "$URL"
    curl -ksI "$URL" 2>&1 | grep -i Last-Modified > ${SCRIPT/sh/timestamp}
    [ -f _.sed ] && sed -i.bak -f _.sed ${SCRIPT/sh/m3u}
}

# MAIN

# Detect the environment
SCRIPT="$0"
cd $(dirname $SCRIPT)

# Download a playlist if absent and create a timestamp mark if nee§
if ! [ -f ${SCRIPT/sh/m3u} ] || ! [ -f ${SCRIPT/sh/timestamp} ]
then
    down_upd
    exit
fi

# Compare the timestamps and update if needed
tsl=$(cat ${SCRIPT/sh/timestamp})
tsr=$( curl -ksI "$URL" 2>&1 | grep -i Last-Modified)
if ! [ "$tsl" = "$tsr" ]
then
    rm ${SCRIPT/sh/m3u}
    down_upd
fi
