#!/bin/bash

# OPTIONS

# Set the location
URL='http://location/of/a/time_period.m3u'

# MAIN

# Detect the environment
SCRIPT="$0"
cd $(dirname $SCRIPT)

# Download a playlist if absent
if ! [ -f ${SCRIPT/sh/m3u} ]
then
    curl -o ${SCRIPT/sh/m3u} "$URL"
    exit
fi

# Check time
# (if they don't provide us with the modification time, we just update the playlist every 2 hours)
if uname -a | grep -iq darwin
then
    modt=$((($(date +%s)-$(stat -f%B ${SCRIPT/sh/m3u}))/3600))
elif uname -a | grep -iq linux
then
    modt=$((($(date +%s)-$(stat -c%Y ${SCRIPT/sh/m3u}))/3600))
fi
if [ $modt -gt 2 ]
then
    rm ${SCRIPT/sh/m3u}
    curl -o ${SCRIPT/sh/m3u} "$URL"
fi
